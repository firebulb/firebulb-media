<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class MobileAppController extends Controller
{
    public function index()
    {
        $projects = Project::whereId(8)->get();
        
        return view('services.mobile', compact('projects'));
    }
}
