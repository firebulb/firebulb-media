<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    public function index()
    {
        $projects = Project::whereId(8)->get();
        
        return view('services.seo', compact('projects'));
    }
}
