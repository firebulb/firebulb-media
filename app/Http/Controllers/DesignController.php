<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function index()
    {
        $projects = Project::whereId(11)->get();
        
        return view('services.design', compact('projects'));
    }
}
