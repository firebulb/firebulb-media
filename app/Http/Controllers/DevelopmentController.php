<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class DevelopmentController extends Controller
{
    public function index()
    {
        $projects = Project::whereId(9)->get();
        
        return view('services.development', compact('projects'));
    }
}
