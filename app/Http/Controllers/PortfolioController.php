<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index()
    {
        $projects = Project::orderBy('date', 'desc')->paginate(12);

        return view('portfolio', compact('projects'));
    }
}
