<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index()
    {
        $projects = Project::whereId(6)->get();
        
        return view('services.store', compact('projects'));
    }
}
