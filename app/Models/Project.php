<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory;

    /**
     * Get the features in an unencoded format.
     *
     * @param  string  $value
     * @return void
     */
    public function getFeaturesAttribute($value)
    {
        return json_decode(json_decode($value, true), true);
    }

    /**
     * Set the slug for the projects client name.
     *
     * @param  string  $value
     * @return void
     */
    public function getClientSlugAttribute($value)
    {
        return Str::slug($this->client);
    }
}
