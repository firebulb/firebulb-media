<?php

return [

    'email' => 'enquiries@firebulb.com.au',
    'phone' => [
        'display' => '1300 170 594',
        'link' => '+611300170594'
    ],
    'address' => 'Albury NSW, 2641',
    'hours' => '10:00AM - 4:00PM',

    'abn' => '59 356 258 879',

    'social' => [
        'facebook' => 'https://www.facebook.com/firebulbmedia',
        'twitter' => 'https://twitter.com/firebulb_media',
        'github' => 'https://github.com/firebulb'
    ]

];
