const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/components/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                varela: ['Varela Round', ...defaultTheme.fontFamily.sans],
            },
            zIndex: {
                '-1': '-1',
            },
            spacing: {
                '108': '27rem',
                '120': '30rem',
                '132': '33rem',
                '144': '36rem',
                '156': '39rem',
                '168': '41rem',
                '180': '44rem',
                'footer': '63rem',
            },
            screens: {
                '3xl': '1600px',
                '4xl': '2000px',
            },
            flex: {
                '7': '7',
            },
            width: {
                '500': '500px',
                '600': '600px',
            },
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
            filter: ['hover', 'focus'],
        },
    },

    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/aspect-ratio'),
        require('@tailwindcss/typography'),
    ],
};
