<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SeoController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\DesignController;
use App\Http\Controllers\MobileAppController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\DevelopmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('home'); });

Route::get('/portfolio', [PortfolioController::class, 'index']);

Route::get('/terms-and-conditions', function () { return view('terms'); });
Route::get('/acceptable-use-policy', function () { return view('acceptable-use'); });

Route::prefix('services')->group(function () {
    Route::get('/website-design', [DesignController::class, 'index']);
    Route::get('/website-development', [DevelopmentController::class, 'index']);
    Route::get('/mobile-app-development', [MobileAppController::class, 'index']);
    Route::get('/ecommerce-online-store-development', [StoreController::class, 'index']);
    Route::get('/graphic-design', function () { return view('services.graphic'); });
    Route::get('/search-engine-optimisation', [SeoController::class, 'index']);
    Route::get('/website-hosting', function () { return view('services.hosting'); });
    // Route::get('/drone-aerial-photography', function () { return view('services.drone'); });
});