<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('company');
            $table->string('code');
            $table->string('address');
            $table->string('town');
            $table->string('state');
            $table->string('postcode');
            $table->string('phone');
            $table->string('stripe_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->string('card_expires_at')->nullable();
            $table->string('trial_ends_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company');
            $table->dropColumn('code');
            $table->dropColumn('address');
            $table->dropColumn('town');
            $table->dropColumn('state');
            $table->dropColumn('postcode');
            $table->dropColumn('phone');
            $table->dropColumn('stripe_id');
            $table->dropColumn('card_brand');
            $table->dropColumn('card_last_four');
            $table->dropColumn('card_expires_at');
            $table->dropColumn('trial_ends_at');
        });
    }
}
