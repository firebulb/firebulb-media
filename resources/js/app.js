require('./bootstrap');

import { createApp } from 'vue';

import Hosting from './components/hosting.vue'
import Reviews from './components/reviews.vue'
import Carousel from './components/carousel.vue'
import DropdownMenu from './components/dropdownMenu.vue'

createApp({
    components: {
        Hosting,
        Reviews,
        Carousel,
        DropdownMenu,
    }
}).mount("#app")