@extends('layouts.master')

@section('meta-title', 'Terms & Conditions | FireBulb Media')
@section('meta-description', 'Please review our provided terms and conditions that stipulate how we work with our clients.')
@section('bg-img', '')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Terms & Conditions
        </x-slot>

        Our development terms and conditions. We reserve the right to update this page without notice.
    </x-page-header>

    <section class="container mx-auto prose mt-56 mb-32">
        <p>Thank you for visiting <b>{{ url('/') }}</b>, a web development agency located in Albury, NSW. We have listed our service terms and conditions on this page. Please view these terms and conditions prior to commencing a project with Firebulb Media.</p>

        <p>By using the Services or accessing the Firebulb Media website, you agree to the latest version of these terms and conditions.</p>

        <h3>Website Design & Website Development</h3>

        <p>These are our terms and conditions related to new website projects commenced with Firebulb Media.</p>
        
        <ul>
            <li>Prior to the commencement of a project, a 50% deposit of the total quoted fee is required. The final amount will be required to be paid once the project has met your satisfaction.</li>
            <li>The initial deposit is not refundable if the design or the development work has been started, and you terminate the contract through no fault of ours.</li>
            <li>Once the website design has been signed off all fees are payable within the stated invoice due date, no cancellations will be accepted, and all services ordered must be paid for in full.</li>
            <li>The client must supply all materials and information required by us to complete the work in accordance with any agreed specification. Such materials may include, but are not limited to, photographs, written content, logos and other printed relevant material. Where there is any delay in supplying these materials to us, it will lead to a delay in the completion of work.</li>
            <li>Following the completion of development we will provide you with temporary access to the site for review.</li>
            <li>Once approved, or deemed approved, work cannot subsequently be rejected, and the contract will be deemed to have been completed. The remaining balance of the project price will become due once the final invoice has been issued.</li>
            <li>The website will be developed to function correctly on the latest versions of the most commonly used browsers at the time of project commencement.</li>
            <li>Compatibility is defined as all critical elements of each page being viewable in each of the aforementioned browsers.</li>
        </ul>

        <h3>Website Care</h3>

        <ul>
            <li>Your website care package will commence once the subscription to your selected service is active.</li>
            <li>If the website is not managed by Firebulb Media you will be required to provide us with website administration details, and if possible server access details to fully manage your website.</li>
            <li>Backups are automated if using Firebulb Media's hosting services. And are stored on a separate, fault tolerant storage system.</li>
            <li>Software updates are provided in a timely manner, once we are aware of the update the upgrade process will occur within the next few days.</li>
            <li>Content updates will take place within as soon as practical after being received from the client.</li>
            <li>Firebulb Media are not liable for any data loss between website backups or a corrupted backup file.</li>
            <li>Firebulb Media are not liable for any financial loss due to website hacking prior to or after an update has occurred.</li>
        </ul>
        

        <h3>Website Hosting Services</h3>

        <ul>
            <li>You will be provided with a link to a secure payment gateway to commence a subscription to our hosting services. You will have the option to select your billing frequency during this time.</li>
            <li>Hosting services are managed by Firebulb Media. We will make every effort to ensure that problems are rectified in a timely manner.</li>
            <li>Your hosting service will not include uptime monitoring or error reporting by default, these are additional services that you will be required to purchase if you require them.</li>
            <li>Firebulb Media are not liable for any financial loss due to server downtime.</li>
            <li>Firebulb Media are not liable for data loss due to hardware malfunction.</li>
            <li>Firebulb Media are not responsible for the content displayed on your website hosting, however if the content does not meet the requirements listed in our <a href="/acceptable-use-policy">acceptable use policy</a>, we reserve the right to suspend your hosting service without prior notice.</li>
            <li>If your account is suspended, or you wish to cancel your hosting service prior to the commencement of the next payment month, the remaining balance of your hosting will be forfeited.</li>
        </ul>

        <h3>Logo & Graphic Design</h3>

        <ul>
            <li>For logo design work, there is a 50% deposit required of the quoted figure that is required to be paid prior to the commencement of the project.</li>
            <li>For logo design services clients will receive three concepts to choose from.</li>
            <li>When the design is completed the client will receive the final graphic design files via email, provided in vector and raster image formats.</li>
            <li>Any further changes after project completion off will need to be quoted and billed separately.</li>
            <li>All graphic designs remain the copyright of the client.</li>
            <li>Print artwork will be provided in a print ready file for the clients use. It is the client’s responsibility to check all copy, contact details, email, website addresses etc. Firebulb Media will not be held responsible legally or otherwise for any undiscovered errors on approved artwork. This includes, but is not limited to: spelling, grammar, colour problems, typing errors or oversights you have not located prior to printing. Please get your printer to provide you with a printed proof and thoroughly check the document and colours before printing.</li>
        </ul>
    </section>
@endsection