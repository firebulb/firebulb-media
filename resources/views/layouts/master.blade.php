<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        
        <title>@yield('meta-title')</title>

        <meta name="author" content="Firebulb Media">
        <meta name="description" content="@yield('meta-description')">

        <meta property="og:locale" content="en_AU" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="@yield('meta-title')" />
        <meta property="og:site_name" content="FireBulb Media" /> 

        <meta property="og:description" content="@yield('meta-description')" />
        <link rel="canonical" href="{{ strtolower(Request::url()) }}" />

        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="icon" href="/img/favicon.png" type="image/x-icon">

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124671473-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-124671473-1');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '220300052882332');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=220300052882332&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->

        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Albury",
                    "addressRegion": "New South Wales",
                    "postalCode": "2640"
                },
                "email": "{{ config('business.email')}}",
                "legalName": "FireBulb Media",
                "url": "{{ env('APP_URL') }}",
                "contactPoint": [{
                    "@type": "ContactPoint",
                    "telephone": "{{ config('business.phone.display') }}",
                    "contactType": "customer service"
                }],
                "logo": "{{ asset('images/logo.svg') }}"
            }
        </script>
    </head>
    <body class="@yield('bg-img')">

        <!-- Messenger Chat Plugin Code -->
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                xfbml            : true,
                version          : 'v10.0'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Your Chat Plugin code -->
        <div class="fb-customerchat"
        attribution="page_inbox"
        page_id="963560810485158">
        </div>

        <div id="app" class="overflow-hidden">

            <header class="relative z-10 bg-gray-800 h-20 w-full px-3 md:px-10">
                <div class="container mx-auto flex items-center justify-between h-full">
                    <div>
                        <a href="/" class="flex items-end">
                            <object class="h-10 w-8" data="/img/icons/flame.svg" type="image/svg+xml" aria-label="Firebulb flame" ></object>
                            <img class="ml-3" src="/img/logo.svg" alt="Firebulb Media Logo" />
                        </a>
                    </div>
                    <nav class="flex items-center">
                        <div>
                            <a href="/portfolio" class="text-white font-semibold transition duration-300 transition-colors hover:text-green-300">Portfolio</a>
                        </div>

                        <dropdown-menu></dropdown-menu>
                        
                        <div class="hidden md:flex md:items-center md:ml-6 lg:ml-10">
                            <div class="hidden md:block lg:hidden">
                                <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20">
                                    <path stroke="#6EE7B7" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 3a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V17a2 2 0 01-2 2h-1C7.716 19 1 12.284 1 4V3z"/>
                                </svg>
                            </div>
                            <div class="hidden lg:block">
                                <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 24 24">
                                    <path fill="#3CD3AD" fill-rule="evenodd" d="M9.659.23A12 12 0 0124 12v1.8a4.2 4.2 0 01-7.675 2.359A6 6 0 1118 12v1.8a1.8 1.8 0 103.6 0V12a9.6 9.6 0 10-9.6 9.6h.001a9.549 9.549 0 004.797-1.286 1.2 1.2 0 111.204 2.077A11.95 11.95 0 0111.999 24L12 22.8V24h-.001A12 12 0 019.659.23zM15.6 12a3.6 3.6 0 10-7.2 0 3.6 3.6 0 007.2 0z" clip-rule="evenodd"/>
                                </svg>
                            </div>
                            <div class="text-white ml-2">
                                <p class="text-sm">Contact Us</p>
                                <p class="hidden md:block lg:hidden font-bold">
                                    <a href="tel:{{ config('business.phone.link') }}" class="text-white transition duration-300 transition-colors hover:text-green-300">{{ config('business.phone.display') }}</a>
                                </p>
                                <p class="hidden lg:block font-bold">
                                    <a href="mailto:{{ config('business.email') }}" class="text-white transition duration-300 transition-colors hover:text-green-300">{{ config('business.email') }}</a></p>
                            </div>
                        </div>

                    </nav>
                </div>

            </header>

            @yield('content')

            <footer class="relative md:mt-6 lg:mt-3">
                <div class="absolute -z-1 -mx-footer xl:-mx-0">
                    <img src="/img/backgrounds/footer-blob.svg" alt="footer blob" />
                </div>

                <div class="hidden md:block absolute md:right-10 md:bottom-28 lg:right-1 -z-1">
                    <img class="w-64" src="/img/icons/social-arrow.svg" alt="Social Arrow" />
                </div>

                <div class="relative container mx-auto">

                    <div class="absolute -top-6 left-1/2 transform -translate-x-1/2 md:left-auto md:translate-x-auto md:right-0 md:-top-2 lg:top-10 lg:right-10 2xl:top-16 4xl:top-20">
                        <div class="flex justify-center">
                            <a href="{{ config('business.social.facebook') }}" target="_blank" rel="noreferrer nofollow" aria-label="Facebook" class="w-16 h-16 flex items-center justify-center bg-green-300 text-green-800 rounded-full duration-300 transform hover:rotate-6 hover:text-green-900 hover:bg-green-400 md:mt-8 lg:mt-6 xl:mt-0">
                                <img class="w-8 h-8" src="/img/icons/facebook.svg" alt="facebook" />
                            </a>
                            <a href="{{ config('business.social.twitter') }}" target="_blank" rel="noreferrer nofollow" aria-label="Twitter" class="w-16 h-16 ml-3 flex items-center justify-center bg-green-300 text-green-800 rounded-full mt-2 md:mt-12 lg:mt-8 xl:mt-2 duration-300 transform hover:rotate-6 hover:text-green-900 hover:bg-green-400">
                                <img class="w-8 h-8" src="/img/icons/twitter.svg" alt="twitter" />
                            </a>
                            <a href="{{ config('business.social.github') }}" target="_blank" rel="noreferrer nofollow" aria-label="Instagram" class="w-16 h-16 ml-3 flex items-center justify-center bg-green-300 text-green-800 rounded-full mt-4 md:mt-16 lg:mt-10 xl:mt-0 duration-300 transform hover:rotate-6 hover:text-green-900 hover:bg-green-400">
                                <img class="w-8 h-8" src="/img/icons/github.svg" alt="github" />
                            </a>
                        </div>
                    </div>                    

                    <div class="pt-16 pb-10 px-4 container mx-auto">
                        <div class="md:grid md:grid-cols-2 md:gap-4 md:py-10 lg:pt-24 lg:pb-10 4xl:pt-24 4xl:pb-10 lg:grid-cols-3">
                            <div class="col-span-2 lg:col-span-1 lg:px-10">
                                <h3 class="text-2xl text-green-500 font-varela">About Us</h3>
                                <div class="mt-4 text-green-800">
                                    <p>FireBulb media are your local web professionals, we have been building websites, applications, and mobile apps for the last 10 years.</p>
                                    <p class="mt-4">We love the challenge that software development provides and look forward to bringing your next project to life!</p>
                                </div>
                            </div>
                            <div class="mt-6 md:mt-0 4xl:px-10">
                                <h3 class="text-2xl text-green-500 font-varela">Our Services</h3>
                                <ul class="mt-4 text-green-800">
                                    <li class="flex items-center">
                                        <img class="w-5 h-5" src="/img/icons/chip.svg" alt="chip" />
                                        <a href="/services/website-design" class="ml-2 transition duration-300 hover:text-green-600">Website Design Albury</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/chip.svg" alt="chip" />
                                        <a href="/services/website-development" class="ml-2 transition duration-300 hover:text-green-600">Web Development Albury</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/chip.svg" alt="chip" />
                                        <a href="/services/mobile-app-development" class="ml-2 transition duration-300 hover:text-green-600">Mobile App Development Albury</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/chip.svg" alt="chip" />
                                        <a href="/services/ecommerce-online-store-development" class="ml-2 transition duration-300 hover:text-green-600">eCommerce Stores Albury</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/chip.svg" alt="chip" />
                                        <a href="/services/website-hosting" class="ml-2 transition duration-300 hover:text-green-600">Website Hosting Albury</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="mt-6 md:mt-0 4xl:px-10">
                                <h3 class="text-2xl text-green-500 font-varela">Get In Touch</h3>
                                <ul class="mt-4 text-green-800">
                                    <li class="flex items-center">
                                        <img class="w-5 h-5" src="/img/icons/house.svg" alt="address" />
                                        <span class="ml-2">{{ config('business.address') }}</span>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/phone.svg" alt="phone number" />
                                        <a href="tel:{{ config('business.phone.link') }}" class="ml-2 transition duration-300 hover:text-green-600">{{ config('business.phone.display') }}</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/mail.svg" alt="email" />
                                        <a href="mailto:{{ config('business.email') }}" class="ml-2 transition duration-300 hover:text-green-600">{{ config('business.email') }}</a>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/time.svg" alt="business hours" />
                                        <span class="ml-2">Mon - Fri ~ {{ config('business.hours') }}</span>
                                    </li>
                                    <li class="flex items-center mt-2">
                                        <img class="w-5 h-5" src="/img/icons/business.svg" alt="abn" />
                                        <span class="ml-2">ABN: {{ config('business.abn') }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-800 text-white text-sm text-center p-4 font-semibold">
                    <div class="container mx-auto lg:flex lg:justify-between">
                        <div class="md:flex md:justify-between lg:items-center">
                            <div class="flex justify-center items-center">
                                <div class="hidden md:block h-6 w-4">
                                    <object data="/img/icons/flame.svg" type="image/svg+xml" aria-label="Firebulb Flame"></object>
                                </div>
                                <p class="md:ml-2">© 2021 Firebulb Media. All Rights Reserved.</p>
                            </div>
                            <p class="mt-2 md:mt-0 lg:ml-2">
                                <a href="/terms-and-conditions" class="text-white transition duration-200 transition-colors hover:text-green-300">Terms & Conditions</a> - 
                                <a href="/acceptable-use-policy" class="text-white transition duration-200 transition-colors hover:text-green-300">Acceptable Use Policy</a>.
                            </p>
                        </div>
                        <div class="hidden lg:block">
                            <p>Website & Mobile App Development In Albury Wodonga</p>
                        </div>
                    </div>
                </div>
            </footer>

        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
