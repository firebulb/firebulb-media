<div class="md:mt-0 tile transition duration-300 relative bg-green-50 rounded-lg p-4 border border-4 border-green-100">
    <div class="flex justify-center absolute -top-14 left-1/2 transform -translate-x-1/2">
        <div class="icon transition duration-300 w-28 h-28 rounded-full bg-green-300 flex items-center justify-center border border-4 border-green-200">
            <img class="w-16 h-16" src="/img/icons/{{ $icon }}.svg" alt="{{ $title }}" />
        </div>
    </div>
    <div class="mt-16 text-center">
        <div class="font-varela text-lg text-green-800 md:text-xl">{{ $title }}</div>
        <p class="mt-4 text-green-700 md:text-sm text-justify">{{ $slot }}</p>
    </div>
</div>