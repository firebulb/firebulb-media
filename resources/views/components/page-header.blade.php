<section class="relative p-4">
    <div class="absolute -mx-24 -top-40 sm:-top-108 md:mx-0 md:mr-10 md:-top-64 md:left-0 md:mr-20 lg:mr-40 lg:-top-96 xl:-top-156">
        <img src="/img/backgrounds/header-blob-sm.svg" alt="header blob" />
    </div>

    <div class="relative container mx-auto pt-4 pb-6">
        <div class="text-center md:text-left md:flex">
            <div class="md:pr-40 lg:pr-0 lg:max-w-2xl">
                <div class="block md:flex md:items-center">
                    <h1 class="lg:mt-4 text-3xl lg:text-4xl text-green-900 font-varela">{{ $title }}</h1>
                </div>
                <p class="mt-2 md:mt-4 text-green-900 text-base">{{ $slot }}</p>
            </div>
        </div>
    </div>
</section>