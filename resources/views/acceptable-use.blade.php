@extends('layouts.master')

@section('meta-title', 'Acceptable Use Policy | FireBulb Media')
@section('meta-description', 'Please review our hosting services acceptable use policy.')
@section('bg-img', '')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Acceptable Use Policy
        </x-slot>

        This policy outlines our acceptable use guidelines for our hosting services. We reserve the right to update this page without notice.
    </x-page-header>

    <section class="container mx-auto prose mt-56 mb-32">
        <p>This Acceptable Use Policy describes prohibited uses of the services offered by <a href="{{ url('/') }}">{{ url('/') }}</a>. (the “Services”) and the website located at <a href="{{ url('/') }}">{{ url('/') }}</a> (the “Firebulb Media website”). The examples described in this Policy are not exhaustive. We may modify this Policy at any time by posting a revised version on the Firebulb Media website.</p>
        <p>By using the Services or accessing the Firebulb Media website, you agree to the latest version of this policy. If you violate the Policy or authorise or help others to do so, we may suspend or terminate your use of our website hosting services.</p>

        <h3>Illegal or Offensive Content</h3>
        <p>You may not use, or encourage, promote, facilitate or instruct others to use, the Services or Firebulb Media website for any illegal, harmful, fraudulent, infringing or offensive use, or to transmit, store, display, distribute or otherwise make available content that is illegal, harmful, fraudulent, infringing or offensive. Prohibited activities or content include, but are not limited to:</p>

        <ul>
            <li><b>Illegal, Harmful or Fraudulent Activities</b> - Any activities that are illegal, that violate the rights of others, or that may be harmful to others, our operations or reputation, including disseminating, promoting or facilitating child pornography, offering or disseminating fraudulent goods, services, schemes, or promotions, make-money-fast schemes, or ponzi and pyramid schemes.</li>
            <li><b>Infringing Content</b> - Content that infringes or misappropriates the intellectual property or proprietary rights of others.</li>
            <li><b>Offensive Content</b> - Content that is defamatory, obscene, abusive, invasive of privacy, or otherwise objectionable, including content that is pornographic in nature or discriminatory against any race, religion, or creed.</li>
            <li><b>Harmful Content</b> - Content or other computer technology that may damage, interfere with, surreptitiously intercept, or expropriate any system, program, or data, including viruses, Trojan horses or worms.</li>
        </ul>

        <h3>Network Abuse</h3>

        <p>You may not make network connections to any users, hosts, or networks unless you have permission to communicate with them. Prohibited activities include, but are not limited to:</p>

        <ul>
            <li><b>User Solicitation</b> - Soliciting login credentials from another user or using or attempting to use another user’s account, username, or password without their permission. Monitoring or Crawling. Monitoring or crawling of a System that impairs or disrupts the System being monitored or crawled.</li>
            <li><b>Intentional Interference</b> - Interfering with the proper functioning of the Services, including any deliberate attempt to overload a network or System.</li>
            <li><b>Avoiding System Restrictions</b> - Using manual or electronic means to avoid any use limitations placed on a System, such as access, message, seat, API, bot, or contact restrictions.</li>
        </ul>

        <h3>Security Violations</h3>

        <p>You may not violate the security or integrity of any network, computer or communications system, software application, or network or computing device (each, a “System”). Prohibited activities include, but are not limited to:</p>

        <ul>
            <li><b>Unauthorised Access</b> - Accessing or using any System without permission, including attempting to probe, scan, or test the vulnerability of a System or to breach any security or authentication measures used by a System.</li>
            <li><b>Interception</b> - Monitoring of data or traffic on a System without permission.</li>
            <li><b>Third Party Services</b> - Developing any applications that interact with the Services or other users’ content or information without our written consent.</li>
        </ul>

        <h3>Monitoring and Enforcement</h3>

        <p>We reserve the right, but do not assume the obligation, to investigate any violation of this Policy or misuse of the Services or Firebulb Media Website. We may:</p>

        <ul>
            <li>Investigate violations of this policy or misuse of our hosting services</li>
            <li>Remove or disable any content or resource that violates this policy or any other agreement we have with you for use of our hosting services.</li>
        </ul>

        <h3>Reporting Violations</h3>

        <p>If you become aware of any violation of this Policy, please <a href="mailto:{{ config('business.email') }}">contact us</a> immediately and provide us with assistance, as requested, to stop or remedy the violation.</p>

    </section>
@endsection