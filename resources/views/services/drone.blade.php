@extends('layouts.master')

@section('meta-title', 'Drone Photography & Videography Albury Wodonga | FireBulb Media')
@section('meta-description', 'If you\'re in need of drone photography and videography in Albury Wodonga, FireBulb can assist you with our CASA licenced drone operators.')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            CASA Licenced Drone Operator
        </x-slot>

        Ensure you get the best aerial content with a CASA licenced drone operator that is also a licenced commercial pilot. We can conduct aerial photography or videography with modern aerial viewing platforms.
    </x-page-header>

    <section class="relative container mx-auto mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-36 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_paper_plane.png" srcset="/img/ed_paper_plane@2x.png 2x" alt="Ed Paper Plane" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">We're Even Professionals in the Aerial Imagery Space Too!</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>If you’re in need of aerial imagery or video, we can help. We are licenced drone operators with a background in aviation, with our operator holding a commercial pilot licence as well as an Remote Pilot Licence.</p>

                    <p class="mt-3">We are capable of providing high quality aerial imagery and video files for your marketing or other needs. Contact us to book a flight and we’ll capture the perfect shots for you.</p>

                    <p class="mt-3">Whether you need imagery, a general aerial inspection or a photogrammetry 3D model, our licenced operators can do it all.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-32 p-4 grid grid-cols-1 gap-20 grid-rows-1 md:grid-cols-3 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:gap-y-20 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Aerial Photography
            </x-slot>
            <x-slot name="icon">
                photos
            </x-slot>

            Capture your product from a different perspective with a high quality aerial photograph from our advanced imaging drone.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Aerial Videography
            </x-slot>
            <x-slot name="icon">
                videos
            </x-slot>

            If you require aerial imagery for a video, we can capture aerial video for you as well. Our quality camera ensure you get the best quality video.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Photogrammetry
            </x-slot>
            <x-slot name="icon">
                photogrammetry
            </x-slot>

            We can capture and create 3D models using photogrammetry. Excellent for generating a 3D landscape or measure a building site.
        </x-tile>
    </section>

    <section class="p-4 relative mb-32 md:mb-48">

        <div class="absolute -z-1 -mx-120 top-10 right-0 md:top-24 md:right-10 lg:mx-0 lg:top-0 lg:right-0 2xl:-top-10">
            <object class="w-[1187px] h-[782px]  2xl:w-[1320px] 2xl:h-[842px] 4xl:w-[1536px] 4xl:h-[782px]" data="/img/backgrounds/gray-blob.svg" type="image/svg+xml" aria-label="Section gray blob"></object>
        </div>

        <div class="container mx-auto lg:flex lg:mt-56 2xl:mt-48 4xl:px-80">
            <div class="flex-shrink-0 items-center md:w-120 lg:w-80 xl:w-180 md:mx-auto">
                <img class="self-center drone-move" src="/img/drone.png" srcset="/img/drone@2x.png 2x" alt="Drone" />
            </div>

            <div class="relative container">
                <div class="md:pl-24 md:flex lg:pl-10 xl:pl-32 2xl:pl-24 2xl:w-3/4 4xl:w-full">
                    <div class="ml-10 md:mt-4">
                        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl md:text-left lg:max-w-xl font-varela">
                        We Use The Latest Most 
                            <span class="inline-block relative">
                                Advanced 
                                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
                            </span>
                            Aerial Platforms
                        </h2>

                        <div class="mt-4 text-gray-600">
                            <p>If you need aerial photographs to showcase your brand or product, we are capable of conducting aerial videography and aerial photography to give a different prospective on your product. We can also create 3D models from photogrammetry if you that is desired. We are CASA certified drone operators, as well as commercial pilots. Be rest assured your imagery will be collected safely by a professional.</p>
                            <p class="mt-3">We utilise the latest and greatest drone technology from DJI, which offers superior quality, 30 minute capture time and sophisticated software to ensure we get the best shots possible.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section class="p-4 lg:container lg:mx-auto mt-72 mb-[-9rem] relative -top-48 md:mt-24 md:-top-10 xl:px-48">
        <div class="md:flex relative">

            <div class="hidden md:block md:relative md:-left-20 md:w-96 lg:-left-0 md:-z-1 md:flex-shrink-0 4xl:mx-40">
                <img src="/img/ed_on_phone.png" srcset="/img/ed_on_phone@2x.png 2x" alt="Ed On Phone" />
            </div>

            <div class="md:flex md:flex-col md:justify-center md:-ml-32 lg:-ml-0 px-4 md:px-0">
                <h3 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl md:text-left lg:max-w-lg font-varela">
                    Give Us A
                    <span class="inline-block relative">
                        <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
                        Call Today
                    </span>

                    For More Information!
                </h3>

                <p class="mt-2 text-gray-500 mx-auto md:mt-6">Give us call to discuss your needs and we'll help you with capture your aerial imagery project.</p>

                <div class="flex justify-center md:hidden w-64 mx-auto mt-4">
                    <img src="/img/ed_on_phone.png" alt="Firebulb Ed On Phone" />
                </div>
                
                <div class="relative md:flex md:items-end -top-3 md:top-0 md:mt-6">
                    <a href="mailto:{{ config('business.email') }}" class="block text-center text-lg font-extrabold bg-green-300 border-4 border-green-300 text-green-800 py-2 px-4 rounded-md md:px-10 transition duration-300 hover:bg-green-400 hover:text-green-900 hover:border-green-400">Contact Us</a>
                    <a href="tel:{{ config('business.phone.link') }}" class="block mt-2 text-center text-lg font-extrabold border-4 border-gray-800 text-gray-800 py-2 px-4 rounded-md md:ml-4 md:px-10 transition duration-300 hover:text-white hover:bg-gray-800">Call: {{ config('business.phone.display') }}</a>
                </div>
                
            </div>
        </div>
    </section>
@endsection