@extends('layouts.master')

@section('meta-title', 'Search Engine Optimisation Albury Wodonga | FireBulb Media')
@section('meta-description', 'FireBulb can assist you with your on-page seo performance, we can audit your website to find improvements to your website seo.')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Search Engine Optimisation
        </x-slot>

        We can help optimise your websites search engine performance to get you above the competition. Our website optimisations involve website content, performance and server configuration tweaks to get your site in tip-tip shape.
    </x-page-header>

    <section class="relative container mx-auto mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-36 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_target.png" srcset="/img/ed_target@2x.png 2x" alt="Ed With Target" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Nail Your Website Performance Targets With FireBulb</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>We can help increase your website traffic and sales with a tailor made SEO solution for your business. Initially we audit your website for low-hanging fruit that can come in the form of poor optimisation, poor content writing and poor website structure.</p>

                    <p class="mt-3">If you need help with a marketing campaign for social media, we can assist in writing content, designing marketing material and scheduling posts or even writing website blog posts to keep your customers engaged and website content fresh. We can also help set up targeted advertisements on social media and google to get an initial performance boost. Want to know the best part? If you build a website with FireBulb, these optimisations are on the house.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-32 p-4">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto font-varela">
            Take A Look At Our 
            <span class="inline-block relative">
                Optimisation Process
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             In Action
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">Let us show you how easy the process is!</p>

        <div class="mt-10 md:grid md:grid-cols-2 md:gap-2 2xl:grid-cols-4 2xl:gap-4">
            <div>
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-1.svg" alt="Step 1" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Discussion</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">This initial phase is where we discuss your needs, as well as get an understanding of your business and the outcome you want to achieve with your website optimisations.</p>
                    </div>
                    <img src="/img/backgrounds/blob-1.svg" alt="Blob 1"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-2.svg" alt="Step 2" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Research</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">We'll start by conducting some research on your industry and competitors to ensure the best strategy to ensure you're seen on the first page.</p>
                    </div>
                    <img src="/img/backgrounds/blob-2.svg" alt="Blob 2"/>
                </div>
            </div>

            <div class="mt-10">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-3.svg" alt="Step 3" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Audit</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">After the initial research is complete, we will conduct and audit on your existing site to determine what work needs to be done to better optimise its search performance.</p>
                    </div>
                    <img src="/img/backgrounds/blob-3.svg" alt="Blob 3"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-4.svg" alt="Step 4" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Optimisation</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">After the audit has been completed, we will begin on optimisation work to give your website the performance boost its needs to get you recognised by your customers.</p>
                    </div>
                    <img src="/img/backgrounds/blob-4.svg" alt="Blob 4"/>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-20 p-4 grid grid-cols-1 gap-20 grid-rows-1 md:grid-cols-3 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:gap-y-20 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Content Creation
            </x-slot>
            <x-slot name="icon">
                content
            </x-slot>

            If you need help writing content for your site that targets specific keywords, but doesn’t feel jarring to read, we can help!
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Site Optimisations
            </x-slot>
            <x-slot name="icon">
                optimisations
            </x-slot>

            Poor site optimisation leads to slow loading, which can have an effect on your sites SERP performance. We optimise both websites, and servers.        
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Marketing Strategy
            </x-slot>
            <x-slot name="icon">
                strategy
            </x-slot>

            We can assist you in finding an effective strategy that will ensure your website performs well both organically and with advertisements.
        </x-tile>


    </section>

    <section class="container mx-auto py-4 px-4 xl:px-20 2xl:px-0 mt-24 mb-36">
        <h2 class="font-varela text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto">
            Here's One Of Our  
            <span class="inline-block relative">
                Recent
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             Search Engine Optimisation Projects
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">See the quality of our work for yourself!</p>

        <div class="mt-10">
            @foreach($projects as $index => $project)
            <div class="px-0 3xl:px-48 4xl:px-72 @if(!$loop->first) mt-10 @endif">
                
                <div class="relative block lg:flex">
                    <img class="hidden xl:block absolute bottom-0 left-[-145px] z-10" src="/img/ed_side.png" srcset="/img/ed_side@2x.png 2x" alt="Ed Pointing Side" />
                    
                    <div>
                        <Carousel project="{{ $project->client_slug }}" :slide-count="{{ $project->slides }}"></Carousel>
                    </div>
                    <div class="mt-6 lg:ml-10 lg:mt-0">
                        <h3 class="text-3xl text-gray-800 font-varela">{{ $project->client }}</h3>
                        <div class="text-green-400 font-bold mt-2">{{ $project->date }}</div>
                        <div class="mt-4 text-gray-600">
                            {!! $project->description !!}
                        </div>

                        <ul class="font-bold mt-4 text-sm">
                            @foreach($project->features as $feature)
                            <li class="flex items-center @if(!$loop->first) mt-2 @endif">
                                <img class="w-6 h-6" src="/img/svg/{{ $feature['icon'] }}" alt="Feature Icon" />
                                <span class="ml-2">{{ $feature['description'] }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <p class="mt-20 text-center text-lg text-gray-500">
            Want to see some more of our work? <a href="/portfolio" class="text-green-600 hover:underline font-bold">Check out our portfolio</a>
        </p>
    </section>
@endsection