@extends('layouts.master')

@section('meta-title', 'Website Design Services Albury Wodonga | FireBulb Media')
@section('meta-description', 'FireBulb Media provide professional website design services in Albury Wodonga and surrounding areas. We are your all-in-one solution for your website')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Website Designers in Albury Wodonga
        </x-slot>

        We are professional website designers situated in Albury Wodonga. We pride ourselves on our modern designs purpose built specifically for the needs of our clients.
    </x-page-header>

    <section class="relative container mx-auto mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-36 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_attention.png" srcset="/img/ed_attention@2x.png 2x" alt="Ed Megaphone" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Get Attention Online With A Website Designed Specifically For Your Needs</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>Give your brand the love it deserves with a beautifully designed, professional website that'll be sure to grab the attention of your customers.</p>

                    <p class="mt-3">FireBulb understands that you only have a few seconds to leave a positive impression before customers return to the search results. We can assist you in providing a good user experience that will make potential customers pick you every time. We provide high quality website designs that perfectly complement your branding, giving you a professional online platform that people can trust.</p>

                    <p class="mt-3">Give your business the competitive edge with a professionally designed website from FireBulb.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-32 p-4">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-md mx-auto font-varela">
            Take A Look At Our 
            <span class="inline-block relative">
                Design Process
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             In Action
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">Let us show you how easy the process is!</p>

        <div class="mt-10 md:grid md:grid-cols-2 md:gap-2 2xl:grid-cols-4 2xl:gap-4">
            <div class="">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-1.svg" alt="Step 1" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Discussion</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">This initial phase is where we discuss your needs, as well as get an understanding of your business and your vision for the project. You’ll have an opportunity to ask us any of your burning questions as well.</p>
                    </div>
                    <img src="/img/backgrounds/blob-1.svg" alt="Blob 1"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-2.svg" alt="Step 2" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Proposal</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once we’ve covered everything we need to know about the project, we’ll write up your proposal document which outlines the requirements of the project and an itemised list of the costs involved in bringing your site to fruition.</p>
                    </div>
                    <img src="/img/backgrounds/blob-2.svg" alt="Blob 2"/>
                </div>
            </div>

            <div class="mt-10">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-3.svg" alt="Step 3" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Design</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once you’re happy with the proposal, we’ll collect any required images and content from you to get started. You'll receive updates and previews throughout the development of your website.</p>
                    </div>
                    <img src="/img/backgrounds/blob-3.svg" alt="Blob 3"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-4.svg" alt="Step 4" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Feedback</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once the site is nearing completion, we’ll give you access to the final version where you can test features and ensure you’re happy with the outcome. Once you’re happy we’ll release the project into the wild for your audience to enjoy.</p>
                    </div>
                    <img src="/img/backgrounds/blob-4.svg" alt="Blob 4"/>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-20 p-4 grid grid-cols-1 gap-20 grid-rows-2 md:grid-cols-2 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:gap-y-20 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Utilising The Latest Tech
            </x-slot>
            <x-slot name="icon">
                tech
            </x-slot>

            We use only the latest bleeding edge technology and techniques to give our customers the best product possible, with industry leading features.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Designed To Get Attention
            </x-slot>
            <x-slot name="icon">
                attention
            </x-slot>

            Our designs focus on getting the attention of your audience. We thoroughly plan content placement to make your site easy to use and appealing.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Analytics Included
            </x-slot>
            <x-slot name="icon">
                binoculars
            </x-slot>

            We set up Google Analytics with all our websites, so you can track user engagement and website usage, allowing for easier optimisation of content.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Optimised For Fast Loading
            </x-slot>
            <x-slot name="icon">
                fast
            </x-slot>

            Website speed is important, which is why we use the latest industry techniques to ensure your website is blazing fast for your audience.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Common Browser Support
            </x-slot>
            <x-slot name="icon">
                browsers
            </x-slot>

            We test our websites on all modern browsers, including Chrome, Firefox, Edge and Safari to ensure a consistent experience for all your users.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Responsive Designs
            </x-slot>
            <x-slot name="icon">
                responsive
            </x-slot>

            We ensure our websites are built perfectly for any device, regardless of its size. So your users can have a functional experience on any device.
        </x-tile>
    </section>

    <section class="container mx-auto py-4 px-4 xl:px-20 2xl:px-0 mt-24 mb-36">
        <h2 class="font-varela text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-md mx-auto">
            Here's One Of Our  
            <span class="inline-block relative">
                Recent
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             Website Design Projects
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">See the quality of our work for yourself!</p>

        <div class="mt-10">
            @foreach($projects as $index => $project)
            <div class="px-0 3xl:px-48 4xl:px-72">
                
                <div class="relative block lg:flex">
                    <img class="hidden xl:block absolute bottom-0 left-[-145px] z-10" src="/img/ed_side.png" srcset="/img/ed_side@2x.png 2x" alt="Ed Pointing Side" />
                    
                    <div>
                        <Carousel project="{{ $project->client_slug }}" :slide-count="{{ $project->slides }}"></Carousel>
                    </div>
                    <div class="mt-6 lg:ml-10 lg:mt-0">
                        <h3 class="text-3xl text-gray-800 font-varela">{{ $project->client }}</h3>
                        <div class="text-green-400 font-bold mt-2">{{ $project->date }}</div>
                        <div class="mt-4 text-gray-600">
                            {!! $project->description !!}
                        </div>

                        <ul class="font-bold mt-4 text-sm">
                            @foreach($project->features as $feature)
                            <li class="flex items-center @if(!$loop->first) mt-2 @endif">
                                <img class="w-6 h-6" src="/img/svg/{{ $feature['icon'] }}" alt="Feature Icon" />
                                <span class="ml-2">{{ $feature['description'] }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <p class="mt-20 text-center text-lg text-gray-500">
            Want to see some more of our work? <a href="/portfolio" class="text-green-600 hover:underline font-bold">Check out our portfolio</a>
        </p>
    </section>
@endsection