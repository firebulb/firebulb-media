@extends('layouts.master')

@section('meta-title', 'Graphic Design Albury Wodonga | FireBulb Media')
@section('meta-description', 'FireBulb media can assist you with your graphic design needs, we can design brochures, pamphlets, magazines, business cards and digital graphics.')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Graphic Designer In Albury Wodonga
        </x-slot>

        Looking to start up a new brand, or refresh an existing one? We can help make eye-catching branding for your business that’ll get you noticed by your customers.
    </x-page-header>

    <section class="relative container mx-auto mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-36 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_pencil.png" srcset="/img/ed_pencil@2x.png 2x" alt="Ed With Pencil" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Beautiful Modern Graphics Designed For Your Business</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>We take great pleasure in assisting both new and established businesses in creating stylish and modern graphics that captivate customers. Customers gain a sense of professionalism from a website by the style and structure used. Graphics can be a deciding factor whether customers use your services or the services of another company.</p>

                    <p class="mt-3">FireBulb can also produce your physical promotional material, including magazines, pamphlets, brochures, business cards, plus any other printable products you may require. All artwork is provided in a format ready to be printed. We are flexible in meeting the printing requirements for your desired printing agency or service.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-20 xl:mt-32 p-4">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto font-varela">
            Take A Look At Our 
            <span class="inline-block relative">
                Graphic Design Process
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             In Action
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">Let us show you how easy the process is!</p>

        <div class="mt-10 md:grid md:grid-cols-2 md:gap-2 2xl:grid-cols-4 2xl:gap-4">
            <div>
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-1.svg" alt="Step 1" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Discussion</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">This initial phase is where we discuss your needs, as well as get an understanding of your business and your vision for the graphics.</p>
                    </div>
                    <img src="/img/backgrounds/blob-1.svg" alt="Blob 1"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-2.svg" alt="Step 2" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Proposal</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once we’ve covered everything we need to know about the project, we’ll write up your proposal document which outlines the requirements of your graphics and an itemised list of the costs involved in creating your graphics.</p>
                    </div>
                    <img src="/img/backgrounds/blob-2.svg" alt="Blob 2"/>
                </div>
            </div>

            <div class="mt-10">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-3.svg" alt="Step 3" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Development</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once you’re happy with the proposal, we’ll collect any required assets from you to create the assets if needed. You'll receive preview files once the graphics have been completed.</p>
                    </div>
                    <img src="/img/backgrounds/blob-3.svg" alt="Blob 3"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-4.svg" alt="Step 4" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Handover</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once you're happy with the selection of graphics provided, we'll send over the high quality files for you to use in your marketing material, or send to your printing company of choice.</p>
                    </div>
                    <img src="/img/backgrounds/blob-4.svg" alt="Blob 4"/>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-20 p-4 grid grid-cols-1 gap-20 grid-rows-2 md:grid-cols-2 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:gap-y-20 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Logo Design
            </x-slot>
            <x-slot name="icon">
                logos
            </x-slot>

            If you're in need of a modern professional logo we can help establish professional branding with a logo design that will be sure to get you noticed.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Business Card Design
            </x-slot>
            <x-slot name="icon">
                cards
            </x-slot>

            Leave a great first impression on your customers with a professional business card. We can design business cards perfectly suited for your business!
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Pamphlet Design
            </x-slot>
            <x-slot name="icon">
                pamphlets
            </x-slot>

            If you're in need of a smaller pamphlet for your business, we can help with pamphlets for menus, products, services etc. The possibilities are endless
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Magazine Design
            </x-slot>
            <x-slot name="icon">
                magazines
            </x-slot>

            Want to showcase a variety of products, or provide information that spans over multiple pages? We can design print-ready magazines for you.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Brochure Design
            </x-slot>
            <x-slot name="icon">
                brochures
            </x-slot>

            Looking for trifold brochures, we can design a brochure for your product or service that your customers will be sure to keep on the fridge.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Letterhead Design
            </x-slot>
            <x-slot name="icon">
                letters
            </x-slot>

            Give your business a touch of class with a professional letterhead design that is personalised specifically for your business needs.
        </x-tile>

        
    </section>

    <section class="container mx-auto py-4 px-4 xl:px-20 2xl:px-0 mt-24 mb-36">
        <h2 class="font-varela text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto">
            Here's Some Of Our  
            <span class="inline-block relative">
                Recent
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             Graphic Design Work
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">See the quality of our work for yourself!</p>

        <div class="mt-10">
            <div class="px-0 3xl:px-48 4xl:px-72">
                
                <div class="relative block lg:flex">
                    <img class="hidden xl:block absolute bottom-0 left-[-145px] z-10" src="/img/ed_side.png" srcset="/img/ed_side@2x.png 2x" alt="Ed Pointing Side" />
                    
                    <div>
                        <Carousel project="graphic-design" :slide-count="7"></Carousel>
                    </div>
                    <div class="mt-6 lg:ml-10 lg:mt-0">
                        <h3 class="text-3xl text-gray-800 font-varela">Graphic Design</h3>
                        <div class="mt-4 text-gray-600">
                            <p>Preview a selection of some various logos, business cards and pamphlets we have made over the years for companies all around Australia. We like to utilise a minimalistic and clean style in our designs for a professional and modern appearance for our graphic material.</p>
                            <p class="mt-3">We provide high quality marketing material that can be used online, in printable documents, and physical products/merchandise.</p>

                            <ul class="font-bold mt-4 text-sm">
                                <li class="flex items-center">
                                    <img class="w-6 h-6" src="/img/svg/brush.svg" alt="Feature Icon">
                                    <span class="ml-2">Clean minimalistic modern graphic designs.</span>
                                </li>
                                <li class="flex items-center mt-2">
                                    <img class="w-6 h-6" src="/img/svg/brush.svg" alt="Feature Icon">
                                    <span class="ml-2">Print graphics such as business cards, magazines, brochures etc.</span>
                                </li>
                                <li class="flex items-center mt-2">
                                    <img class="w-6 h-6" src="/img/svg/brush.svg" alt="Feature Icon">
                                    <span class="ml-2">Professional logo designs.</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <p class="mt-20 text-center text-lg text-gray-500">
            Want to see some more of our work? <a href="/portfolio" class="text-green-600 hover:underline font-bold">Check out our portfolio</a>
        </p>
    </section>
@endsection