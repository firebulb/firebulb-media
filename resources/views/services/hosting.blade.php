@extends('layouts.master')

@section('meta-title', 'Afforable Website Hosting Albury Wodonga | FireBulb Media')
@section('meta-description', 'We offer a variety of afforable hosting packages specifically for small businesses. Get in touch today to get your site online with us.')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Australian Website Hosting
        </x-slot>

        We use the latest technology to provide a reliable and fast hosting service. We provide quality affordable hosting to all our customers that want an in-house all-in-one solution to keep management simple.
    </x-page-header>

    <section class="relative container mx-auto mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-36 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_cape.png" srcset="/img/ed_cape@2x.png 2x" alt="Ed Superhero" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Blazing Fast High Value Modern Servers Based In Australia</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>Customers expect your website to load in a matter of milliseconds! Our Australian based hosting provides low response times, meaning every server request is superfast!</p>

                    <p class="mt-3">Having servers based in Australia offers many benefits to your customers. Your data remains safely in Australia, and your applications and websites are accessible at lightning speed.</p>

                    <p class="mt-3">Our integrated billing system takes the hassle out of paying for your hosting. Our billing system is secure, and your credit card information is never stored on our servers.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-10 md:mt-32">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-lg mx-auto font-varela">
            Our
            <span class="inline-block relative">
                High-Value
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
            Hosting Monthly Pricing
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">We’ll give you a discount if you pay  for your hosting yearly</p>

        <hosting></hosting>

        <p class="mt-4 text-center text-gray-500">To get your website setup on our servers, <a href="mailto:{{ config('business.email') }}" class="text-green-700 font-bold">get in touch</a>!</p>
    </section>

    <section class="mt-32 mb-32 p-4 grid grid-cols-1 gap-20 grid-rows-1 md:grid-cols-3 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Lightning Fast
            </x-slot>
            <x-slot name="icon">
                fast
            </x-slot>

            Our servers are located in Australia and not overseas, this provides low latency and quick response times for your local customers.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Easily Upgradeable
            </x-slot>
            <x-slot name="icon">
                upgrade
            </x-slot>

            Our plans can be scaled up effortlessly as your website grows to ensure you have the resources you need to for optimal delivery.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Modern Hardware
            </x-slot>
            <x-slot name="icon">
                hardware
            </x-slot>

            Our servers utilise modern hardware such as solid state drives and the latest server grade intel processors to ensure reliability and performance.
        </x-tile>
    </section>
@endsection