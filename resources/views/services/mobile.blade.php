@extends('layouts.master')

@section('meta-title', 'iOS & Android Mobile App Development Albury Wodonga | FireBulb Media')
@section('meta-description', 'If you are in need of a mobile application developer that can develop mobile apps for Android or iOS Developer, Firebulb can help!')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Mobile App Developer In Albury
        </x-slot>

        Need an app that works seamlessly on mobile devices? We specialise in mobile application development, providing mobile applications that work seamlessly on iOS and Android devices.
    </x-page-header>

    <section class="relative container mx-auto mt-32 xl:mt-48">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Header Blob"></object>
        </div>
        <div class="absolute -top-10 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2">
                <img src="/img/ed_phone_lg.png" srcset="/img/ed_phone_lg@2x.png 2x" alt="Ed With Phone" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h2 class="text-lg sm:text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Have The Next Big App? <br> We Can Build It For You!</h2>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>We are experienced app developers, having built Android and iOS applications that work seamlessly on both phones, and tablets.</p>

                    <p class="mt-3">If you need an app for your business, or want to launch something truly special to rival big tech, we can help make it a reality. Our apps are strenuously tested for usability and performance, to ensure an enjoyable experience for your users.</p>

                    <p class="mt-3">Need a custom application that integrates with your app? We can build a custom application and supporting API's that work seamlessly with your app as well.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-20 xl:mt-32 p-4">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto font-varela">
            Take A Look At Our 
            <span class="inline-block relative">
                Development Process
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             In Action
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">Let us show you how easy the process is!</p>

        <div class="mt-10 md:grid md:grid-cols-2 md:gap-2 2xl:grid-cols-4 2xl:gap-4">
            <div>
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-1.svg" alt="Step 1" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Discussion</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">This initial phase is where we discuss your needs, as well as get an understanding of your business and your vision for the project. You’ll have an opportunity to ask us any of your burning questions as well.</p>
                    </div>
                    <img src="/img/backgrounds/blob-1.svg" alt="Blob 1"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-2.svg" alt="Step 2" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Proposal</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once we’ve covered everything we need to know about the project, we’ll write up your proposal document which outlines the requirements of the project and an itemised list of the costs involved in bringing your site to fruition.</p>
                    </div>
                    <img src="/img/backgrounds/blob-2.svg" alt="Blob 2"/>
                </div>
            </div>

            <div class="mt-10">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-3.svg" alt="Step 3" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Development</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once you’re happy with the proposal and ready to continue, we’ll collect any necessary material and begin development of your application. You'll receive updates and previews throughout the development of your web app.</p>
                    </div>
                    <img src="/img/backgrounds/blob-3.svg" alt="Blob 3"/>
                </div>
            </div>

            <div class="mt-10 md:mt-48 xl:mt-64">
                <div class="relative text-center max-w-md mx-auto">
                    <img class="absolute w-36 h-36 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2" src="/img/backgrounds/step-4.svg" alt="Step 4" />
                    <div class="absolute w-full h-full flex flex-col justify-center items-center p-16">
                        <h3 class="text-xl font-varela text-gray-800 md:text-3xl">Feedback</h3>
                        <img class="w-14 mt-2" src="/img/underline.svg" alt="Heading Underline" />
                        <p class="text-sm mt-2 text-gray-600 md:text-base">Once the mobile app is nearing completion, we’ll give you access to the final version where you can test features and ensure you’re happy with the outcome. Once you’re happy we’ll release the project into the wild for your audience or staff to enjoy.</p>
                    </div>
                    <img src="/img/backgrounds/blob-4.svg" alt="Blob 4"/>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-20 p-4 grid grid-cols-1 gap-20 grid-rows-1 md:grid-cols-3 md:gap-y-20 md:gap-5 md:p-4 lg:container lg:mx-auto lg:grid-cols-3 2xl:gap-10 2xl:gap-y-20 2xl:px-48">
        <x-tile>
            <x-slot name="title">
                Utilising The Latest Tech
            </x-slot>
            <x-slot name="icon">
                tech
            </x-slot>

            We use only the latest bleeding edge technology and techniques to give our customers the best product possible, with industry leading features.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Supporting Android & iOS
            </x-slot>
            <x-slot name="icon">
                os
            </x-slot>

            Our apps can work on both iOS and Android, so you can provide your app for users regardless of the device being used.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Quality User Experience
            </x-slot>
            <x-slot name="icon">
                experience
            </x-slot>

            We put a lot of effort into planning the user experience of all our apps, to ensure that your users have the best possible experience when using your app.
        </x-tile>
    </section>

    <section class="container mx-auto py-4 px-4 xl:px-20 2xl:px-0 mt-24 mb-36">
        <h2 class="font-varela text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-xl mx-auto">
            Here's One Of Our  
            <span class="inline-block relative">
                Recent
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
             Website Application Projects
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">See the quality of our work for yourself!</p>

        <div class="mt-10">
            @foreach($projects as $index => $project)
            <div class="px-0 3xl:px-48 4xl:px-72 @if(!$loop->first) mt-10 @endif">
                
                <div class="relative block lg:flex">
                    <img class="hidden xl:block absolute bottom-0 left-[-145px] z-10" src="/img/ed_side.png" srcset="/img/ed_side@2x.png 2x" alt="Ed Pointing Side" />
                    
                    <div>
                        <Carousel project="{{ $project->client_slug }}" :slide-count="{{ $project->slides }}"></Carousel>
                    </div>
                    <div class="mt-6 lg:ml-10 lg:mt-0">
                        <h3 class="text-3xl text-gray-800 font-varela">{{ $project->client }}</h3>
                        <div class="text-green-400 font-bold mt-2">{{ $project->date }}</div>
                        <div class="mt-4 text-gray-600">
                            {!! $project->description !!}
                        </div>

                        <ul class="font-bold mt-4 text-sm">
                            @foreach($project->features as $feature)
                            <li class="flex items-center @if(!$loop->first) mt-2 @endif">
                                <img class="w-6 h-6" src="/img/svg/{{ $feature['icon'] }}" alt="Feature Icon" />
                                <span class="ml-2">{{ $feature['description'] }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <p class="mt-20 text-center text-lg text-gray-500">
            Want to see some more of our work? <a href="/portfolio" class="text-green-600 hover:underline font-bold">Check out our portfolio</a>
        </p>
    </section>
@endsection