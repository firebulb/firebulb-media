@extends('layouts.master')

@section('meta-title', 'Firebulb Media | Website Design & App Development Albury Wodonga')
@section('meta-description', 'FireBulb Media are your local web design and mobile app development team located in Albury Wodonga. Ignite your ideas with FireBulb.')
@section('bg-img', '')

@section('content')
    <section class="relative">
        <div class="absolute -mx-60 -top-64 -left-24 -z-1 md:-top-108 lg:mx-24 lg:-top-24 2xl:-top-40 xl:-mx-0 4xl:-top-108 ">
            <object class="w-[860px] h-[691px] md:w-[990px] md:h-[1200px] lg:w-[1100px] lg:h-[800px] 4xl:w-[1280px] 4xl:h-[1029px]" data="/img/backgrounds/header-blob.svg" type="image/svg+xml" aria-label="Header background image" ></object>
        </div>
        
        <div class="relative container mx-auto p-4">
            <div class="text-center pt-10 md:pt-16 md:text-left md:flex lg:pt-20 xl:pt-24">
                <div class="ml-0 md:ml-8 lg:w-1/2 lg:ml-24 lg:pr-0 xl:w-1/3">
                    <div class="block md:flex md:items-center">
                        <h1 class="relative block text-4xl lg:text-5xl text-green-900 font-varela">
                            <div class="hidden md:block absolute w-[17px] h-[19px] left-[80px] top-[-9px] md:left-[50px] lg:left-[70px]">
                                <object data="/img/icons/flame.svg" type="image/svg+xml" aria-label="Firebulb Flame"></object>
                            </div>
                            Ignite Your Ideas
                        </h1>
                    </div>
                    <p class="mt-3 md:mt-4 text-green-900 md:text-lg lg:text-xl">Lay the kindling for your business and engage your customers with a purpose built website!</p>

                    <ul class="text-left mt-8 text-green-900 font-bold text-sm md:text-base lg:text-lg">
                        <li>
                            <div class="flex items-center">
                                <img class="w-5 h-5" src="/img/icons/check-circle.svg" alt="Check Icon" />
                                <span class="ml-2">We Follow The Latest Industry Standards</span>
                            </div>
                        </li>
                        <li class="mt-4">
                            <div class="flex items-center">
                                <img class="w-5 h-5" src="/img/icons/check-circle.svg" alt="Check Icon" />
                                <span class="ml-2">We’re Based In Albury/Wodonga</span>
                            </div>
                        </li>
                        <li class="mt-4">
                            <div class="flex items-center">
                                <img class="w-5 h-5" src="/img/icons/check-circle.svg" alt="Check Icon" />
                                <span class="ml-2">We Offer Tailor Made Solutions</span>
                            </div>
                        </li>
                        <li class="mt-4">
                            <div class="flex items-center">
                                <img class="w-5 h-5" src="/img/icons/check-circle.svg" alt="Check Icon" />
                                <span class="ml-2">We Provide Affordable Services</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="block mt-10 md:mt-0 -right-32 lg:-right-20 lg:top-10 2xl:right-32 2xl:top-14">
                    <img src="/img/ed_desk.png" srcset="/img/ed_desk@2x.png 2x" alt="Ed At Desk" />
                </div>
            </div>
        </div>
    </section>

    <section class="mt-20 md:mt-64 p-4 grid grid-cols-1 gap-20 md:mt-72 lg:mt-48 md:p-4 md:grid-cols-3 md:grid-rows-1 md:grid-flow-col md:gap-5 lg:container lg:mx-auto xl:gap-10 2xl:px-48">

        <x-tile>
            <x-slot name="title">
                We're Local Developers
            </x-slot>
            <x-slot name="icon">
                local
            </x-slot>

            We’re based in Albury Wodonga and provide local businesses with web development services. We never outsource any of our projects to overseas agencies.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                10 Years Experience
            </x-slot>
            <x-slot name="icon">
                tie
            </x-slot>

            We have been building websites, apps and managing servers for the last 10 years. We’re always learning the latest industry trends to keep ahead of the game.
        </x-tile>

        <x-tile>
            <x-slot name="title">
                Australian Hosting
            </x-slot>
            <x-slot name="icon">
                server
            </x-slot>

            We host our websites in Sydney, which provides fast load times for local users, and peace of mind knowing your data is kept safe in Australia.
        </x-tile>
    </section>

    <section class="relative container mx-auto mt-56 md:mt-24">
        <div class="relative -mx-120 md:-mx-72 xl:mx-10">
            <object data="/img/backgrounds/green-blob.svg" type="image/svg+xml" aria-label="Section background image" ></object>
        </div>
        <div class="absolute -top-40 inset-0 md:flex md:items-center md:justify-center md:top-10 lg:px-10 xl:px-48">
            <div class="relative w-2/3 mx-auto md:w-1/2 md:flex-shrink-0 xl:w-2/5 2xl:w-1/2 4xl:ml-32">
                <img src="/img/ed_seated.png" srcset="/img/ed_seated@2x.png 2x" alt="Ed Seated" />
            </div>
            <div class="px-4 lg:px-0 lg:pr-10">
                <h1 class="text-2xl text-green-900 lg:text-2xl 2xl:text-4xl font-varela">Professional Website Design In Albury Wodonga</h1>
                <div class="mt-4 text-sm lg:text-base text-green-800">
                    <p>We get the web, we’ve been working with web based technologies for the past decade and have the knowledge and skills to create just about anything. No job is too big or small for us!</p>
                    <p class="mt-3">If 2020 has taught us anything, it is that consumers have become reliant on the access of information, products and services online. If you aren’t online now, it is well worth your while to be, and we can help you achieve that.</p>
                    <p class="mt-3">FireBulb media is a local web design agency located in Albury Wodonga that specialises in custom software development. We build websites, web applications, mobile applications and provide hosting services to local businesses.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container mx-auto mt-10 md:mt-24 p-4">
        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl relative lg:max-w-lg mx-auto font-varela">
            We’ve Worked With Countless Businesses
            <span class="inline-block relative">
                All Over Australia!
                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
            </span>
        </h2>
        <p class="mt-2 text-center text-green-700 text-lg font-bold">Let us help you with your online presence</p>

        <div class="flex justify-center grid grid-rows-3 grid-flow-col gap-4 mt-8 md:grid-rows-2 md:gap-14 md:mt-16 lg:gap-24 ">
            <a href="https://inspecteasy.com.au" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/inspecteasy.png" srcset="/img/clients/inspecteasy@2x.png 2x, /img/clients/inspecteasy@3x.png 3x" alt="InspectEasy" />
            </a>
            <a href="https://ngro.com.au" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/ngro.png" srcset="/img/clients/ngro@2x.png 2x, /img/clients/ngro@3x.png 3x" alt="National Garage Remotes & Openers" />
            </a>
            <a href="https://eqtechnologies.com.au/" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/eq.png" srcset="/img/clients/eq@2x.png 2x, /img/clients/eq@3x.png 3x" alt="EQ Technologies" />
            </a>
            <a href="https://mbabuild.com.au" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/mba.png" srcset="/img/clients/mba@2x.png 2x, /img/clients/mba@3x.png 3x" alt="MBA Building Inspections" />
            </a>
            <a href="https://wollongonggaragedoors.com.au" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/wgd.png" srcset="/img/clients/wgd@2x.png 2x, /img/clients/wgd@3x.png 3x" alt="Wollongong Garage Doors" />
            </a>
            <a href="https://canoethemurray.com.au" rel="noreferrer nofollow" target="_blank">
                <img class="filter grayscale transform duration-300 hover:scale-110 hover:filter-none" src="/img/clients/canoemurray.png" srcset="/img/clients/canoemurray@2x.png 2x, /img/clients/canoemurray@3x.png 3x" alt="Canoe The Murray" />
            </a>
        </div>
    </section>

    <section class="p-4 relative">

        <div class="absolute -z-1 -mx-120 top-10 right-0 md:top-24 md:right-10 lg:mx-0 lg:top-0 lg:right-0 2xl:-top-10">
            <object class="w-[1187px] h-[782px]  2xl:w-[1320px] 2xl:h-[842px] 4xl:w-[1536px] 4xl:h-[782px]" data="/img/backgrounds/gray-blob.svg" type="image/svg+xml" aria-label="Section gray background image"></object>
        </div>

        <div class="container mx-auto lg:flex lg:mt-56 2xl:mt-48 4xl:px-80">
            <div class="flex-shrink-0 items-center md:w-120 lg:w-80 xl:w-120 md:mx-auto">
                <img class="self-center" src="/img/ed_with_client.png" srcset="/img/ed_with_client@2x.png 2x" alt="Ed With Client" />
            </div>

            <div class="md:mt-10 lg:mt-0 relative container">
                <div class="mt-6 md:mt-0 md:pl-24 md:flex lg:pl-10 xl:pl-32 2xl:pl-60 2xl:w-3/4 4xl:w-full">
                    <div>
                        <h2 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl md:text-left lg:max-w-md font-varela">
                        Some Reviews From Our
                            <span class="inline-block relative">
                                Happy Customers!
                                <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
                            </span>
                        </h2>

                        <div class="flex justify-center lg:justify-start mt-2">
                            <img class="-ml-2 h-10" src="/img/icons/stars.svg" alt="stars" />
                        </div>

                        <Reviews></Reviews>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section class="p-4 lg:container lg:mx-auto mt-72 mb-[-9rem] relative -top-48 md:mt-24 md:-top-10 xl:px-48">
        <div class="md:flex relative">

            <div class="hidden md:block md:relative md:-left-20 md:w-96 lg:-left-0 md:-z-1 md:flex-shrink-0 4xl:mx-40">
                <img src="/img/ed_on_phone.png" srcset="/img/ed_on_phone@2x.png 2x" alt="Ed On Phone" />
            </div>

            <div class="md:flex md:flex-col md:justify-center md:-ml-32 lg:-ml-0 px-4 md:px-0">
                <h3 class="text-center text-gray-800 text-2xl md:text-3xl lg:text-4xl md:text-left lg:max-w-lg font-varela">
                    Let's 
                    <span class="inline-block relative">
                        <div class="absolute bottom-1 left-0 bg-green-200 rounded h-4 w-full -z-1"></div>
                        Make A Start 
                    </span>

                    On Your Project Today!
                </h3>

                <p class="mt-2 text-gray-500 mx-auto md:mt-6">Let's get this party started! Get in touch today and we’ll work out the best solution for your needs, with a free consultation and quote. Get in touch today.</p>

                <div class="flex justify-center md:hidden w-64 mx-auto mt-4">
                    <img src="/img/ed_on_phone.png" alt="Firebulb Ed On Phone" />
                </div>
                
                <div class="relative md:flex md:items-end -top-3 md:top-0 md:mt-6">
                    <a href="mailto:{{ config('business.email') }}" class="block text-center text-lg font-extrabold bg-green-300 border-4 border-green-300 text-green-800 py-2 px-4 rounded-md md:px-10 transition duration-300 hover:bg-green-400 hover:text-green-900 hover:border-green-400">Contact Us</a>
                    <a href="tel:{{ config('business.phone.link') }}" class="block mt-2 text-center text-lg font-extrabold border-4 border-gray-800 text-gray-800 py-2 px-4 rounded-md md:ml-4 md:px-10 transition duration-300 hover:text-white hover:bg-gray-800">Call: {{ config('business.phone.display') }}</a>
                </div>
                
            </div>
        </div>
    </section>
@endsection