@extends('layouts.master')

@section('meta-title', 'Design Portfolio | FireBulb Media')
@section('meta-description', 'We have been designing website and applications for the last 10 years. View our profile to see some of our work.')
@section('bg-img', 'hexagons')

@section('content')
    <x-page-header>
        <x-slot name="title">
            Our Portfolio
        </x-slot>

        We take pride in all the work we complete, and get just as passionate about your business as you do. View some of our recently completed projects below for ideas on what we can do for your business.
    </x-page-header>

    <section class="container mx-auto my-32 p-4 xl:my-48">
        @foreach($projects as $index => $project)
        <div class="px-0 3xl:px-48 4xl:px-72 @if(!$loop->first) mt-10 @endif">
            
            <div class="relative block lg:flex">

                @if($loop->first)
                    <img class="hidden xl:block absolute -top-48 left-[-129px] z-10" src="/img/ed_leaning.png" srcset="/img/ed_leaning@2x.png 2x" alt="Ed Leaning" />
                @endif

                @if($index == 9)
                    <img class="hidden xl:block absolute bottom-0 left-[-145px] z-10" src="/img/ed_side.png" srcset="/img/ed_side@2x.png 2x" alt="Ed Pointing Side" />
                @endif
                
                <div>
                    <Carousel project="{{ $project->client_slug }}" :slide-count="{{ $project->slides }}"></Carousel>
                </div>
                <div class="mt-6 lg:ml-10 lg:mt-0">
                    @if(isset($project->url))
                        <a class="text-gray-800 hover:text-green-700 duration-300" href="{{ $project->url }}" target="_blank" rel="nofollow"><h3 class="text-3xl font-varela">{{ $project->client }}</h3></a>
                    @else
                        <h3 class="text-3xl text-gray-800 font-varela">{{ $project->client }}</h3>
                    @endif
                    <div class="text-green-400 font-bold mt-2">{{ $project->date }}</div>
                    <div class="mt-4 text-gray-600">
                        {!! $project->description !!}
                    </div>

                    <ul class="font-bold mt-4 text-sm">
                        @foreach($project->features as $feature)
                        <li class="flex items-center @if(!$loop->first) mt-2 @endif">
                            <img class="w-6 h-6" src="/img/svg/{{ $feature['icon'] }}" alt="Feature Icon" />
                            <span class="ml-2">{{ $feature['description'] }}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        
    </section>
@endsection